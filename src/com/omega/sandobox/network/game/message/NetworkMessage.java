/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omega.sandobox.network.game.message;

import com.jme3.network.AbstractMessage;
import com.jme3.network.HostedConnection;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Kyu
 */
@Serializable
public abstract class NetworkMessage extends AbstractMessage {

    public NetworkMessage() {
    }
    
    public abstract void execute(HostedConnection connection);

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
    
}
