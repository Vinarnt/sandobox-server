package com.omega.sandobox.network.game.message;

import com.omega.sandobox.network.game.message.authorization.ServerCapacityMessage;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.sandobox.server.game.data.ClientInstance;
import com.omega.sandobox.network.game.message.authorization.ValidatePasswordMessage;

/**
 *
 * @author Kyu
 */
public class ServerMessageListener implements MessageListener<HostedConnection> {
    
    private static final Logger logger = Logger.getLogger(ServerMessageListener.class.getName());
    
    @Override
    public void messageReceived(HostedConnection source, Message message) {
        if (message instanceof NetworkMessage) {
            logger.log(Level.INFO, "Recv << {0}", message);
            
            if(!(message instanceof ValidatePasswordMessage) && !(message instanceof ServerCapacityMessage))
            {
                ClientInstance _clientInstance = source.getAttribute("clientInstance");
                if(!_clientInstance.getAuthorization().isPasswordVerified())
                {
                    logger.log(Level.WARNING, "Client {0} try to send message without being authorized (Kick)", source.getAddress());
                    source.close("Unauthorized to send message");
                    
                    return;
                }
            }
            ((NetworkMessage) message).execute(source);
        } else
        {
            logger.log(Level.WARNING, "Received unhandled message {0}", message.getClass().getSimpleName());
            source.close("Bad message");
        }
    }
}
