package com.omega.sandobox.network.game.message.authorization;

import com.jme3.network.HostedConnection;
import com.jme3.network.serializing.Serializable;
import com.omega.sandobox.network.game.message.NetworkMessage;

/**
 *
 * @author Kyu
 */
@Serializable
public class WrongPasswordMessage extends NetworkMessage {

    public WrongPasswordMessage() {
    }
    
    @Override
    public void execute(HostedConnection connection) {
    }
    
}
