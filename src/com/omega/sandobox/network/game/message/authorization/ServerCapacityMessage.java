package com.omega.sandobox.network.game.message.authorization;

import com.jme3.network.HostedConnection;
import com.jme3.network.serializing.Serializable;
import net.omega.sandobox.server.config.ConfigurationFactory;
import net.omega.sandobox.server.game.entity.EntityManager;
import com.omega.sandobox.network.game.message.NetworkMessage;

/**
 *
 * @author Kyu
 */
@Serializable
public class ServerCapacityMessage extends NetworkMessage {

    private boolean isFull;

    public ServerCapacityMessage() {
        
    }

    @Override
    public void execute(HostedConnection connection) {
        int _playerCount = EntityManager.getInstance().getPlayerCount();
        isFull = _playerCount >= Integer.valueOf(ConfigurationFactory.getInstance().getProperty("game.players.max"));
        System.out.println("Player count : " + _playerCount);
        
        connection.send(this);
    }
}
