package com.omega.sandobox.network.game.message.authorization;

import com.jme3.network.HostedConnection;
import com.jme3.network.serializing.Serializable;
import com.omega.sandobox.network.game.message.NetworkMessage;

/**
 *
 * @author Kyu
 */
@Serializable
public class VerifiedPasswordMessage extends NetworkMessage {

    public VerifiedPasswordMessage() {
    }

    @Override
    public void execute(HostedConnection connection) {
    }
    
}
