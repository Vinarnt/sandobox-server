package com.omega.sandobox.network.game.message.authorization;

import com.jme3.network.HostedConnection;
import com.jme3.network.serializing.Serializable;
import java.util.Arrays;
import net.omega.sandobox.server.game.ServerFactory;
import net.omega.sandobox.server.game.data.ClientInstance;
import com.omega.sandobox.network.game.message.NetworkMessage;

/**
 *
 * @author Kyu
 */
@Serializable
public class ValidatePasswordMessage extends NetworkMessage {

    private byte[] password;

    public ValidatePasswordMessage() {
    }

    @Override
    public void execute(HostedConnection connection) {
        if (password != null && Arrays.equals(password, ServerFactory.getInstance().getJoinPassword())) {
            {
                ClientInstance _clInst = ((ClientInstance) connection.getAttribute("clientInstance"));
                _clInst.getAuthorization().setPasswordVerified(true);
                _clInst.initializeEntityHandler(connection);
                connection.send(new VerifiedPasswordMessage());
            }
        } else {
            connection.send(new WrongPasswordMessage());
            connection.close("Wrong password");
        }
    }
}
