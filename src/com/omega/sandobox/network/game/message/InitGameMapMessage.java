package com.omega.sandobox.network.game.message;

import com.jme3.network.HostedConnection;
import com.jme3.network.serializing.Serializable;
import net.omega.sandobox.server.config.ConfigurationHandler;

/**
 *
 * @author Kyu
 */
@Serializable
public class InitGameMapMessage extends NetworkMessage {

    private int mapId;
    
    @Override
    public void execute(HostedConnection connection) {
        mapId = ConfigurationHandler.getInstance().getMap();
        
        connection.send(this);
    }
    
}
