package com.omega.sandobox.network.game.message;

import com.jme3.network.HostedConnection;
import com.jme3.network.serializing.Serializable;
import net.omega.sandobox.server.config.ConfigurationHandler;

/**
 *
 * @author Kyu
 */
@Serializable
public class InitGameModeMessage extends NetworkMessage {

    private int gamemode;
    
    @Override
    public void execute(HostedConnection connection) {
        gamemode = ConfigurationHandler.getInstance().getGameMode();
        connection.send(this);
    }
    
}
