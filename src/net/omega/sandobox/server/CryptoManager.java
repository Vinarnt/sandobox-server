package net.omega.sandobox.server;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 *
 * @author Kyu
 */
public class CryptoManager {

    private static final Logger logger = Logger.getLogger(CryptoManager.class.getName());
    private static CryptoManager instance;
    private String algorithm = "PBKDF2WithHmacSHA1";
    private int keyLength = 512;
    private int iterations = 20000;
    private final char[] hexArray = "0123456789abcdef".toCharArray();

    public CryptoManager() {
    }

    public static CryptoManager getInstance() {
        if (instance == null)
            instance = new CryptoManager();

        return instance;
    }

    public byte[] encrypt(String toEncrypt, String useAsSalt) {
        if (toEncrypt.isEmpty()) {
            return null;
        }

        long _startTime = System.nanoTime();
        byte[] _enryptedPassword = getEncryptedPassword(toEncrypt, generateSalt(useAsSalt));
        long _ellapsedTime = System.nanoTime() - _startTime;
        logger.log(Level.INFO, "Password encrypted in {0}", _ellapsedTime * 0.000001);
        return _enryptedPassword;
    }

    private byte[] getEncryptedPassword(String password, byte[] salt) {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, keyLength);
        try {
            SecretKeyFactory _skFactory = SecretKeyFactory.getInstance(algorithm);
            byte[] _securePassword = _skFactory.generateSecret(spec).getEncoded();
            logger.log(Level.INFO, "Encrypted password : {0}", toHex(_securePassword));
            return _securePassword;
        } catch (NoSuchAlgorithmException ex) {
            logger.log(Level.SEVERE, "Encryption algorithm not available", ex);
        } catch (InvalidKeySpecException ex) {
            logger.log(Level.SEVERE, "Unable to generate key", ex);
        }

        return null;
    }

    private byte[] generateSalt(String useAsSalt) {
        StringBuilder _sBuild = new StringBuilder();
        for (int i = 0; i < useAsSalt.length(); i += 2) {
            _sBuild.append(useAsSalt.charAt(i));
        }
        _sBuild.reverse();
        return _sBuild.toString().getBytes();
    }

    private String toHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
