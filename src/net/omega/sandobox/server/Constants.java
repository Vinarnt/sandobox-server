package net.omega.sandobox.server;

/**
 *
 * @author Kyu
 */
public class Constants {

    public static final String MASTER_SERVER_HOST = "localhost";
    public static final int MASTER_SERVER_PORT = 26002;
    public static final int MASTER_SERVER_RECONNECT_DELAY = 5; // In seconds
    public static final String SERVER_NAME = "sandobox";
    public static final int SERVER_VERSION = 1;
    public static final String PATH_SERVER_CONFIG = "server.properties";
}
