package net.omega.sandobox.server;

import net.omega.sandobox.server.game.ServerCore;
import com.jme3.system.JmeContext;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;


public class Main
{

    private static ServerCore core;
    
    public static void main(String[] args)
    {
        LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME).setLevel(Level.FINE);
	core = new ServerCore(true);
        core.start(JmeContext.Type.Headless);
    }

}
