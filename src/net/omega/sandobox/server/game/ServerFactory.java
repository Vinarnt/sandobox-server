package net.omega.sandobox.server.game;

import net.omega.sandobox.server.config.ConfigurationHandler;
import com.jme3.network.ConnectionListener;
import com.jme3.network.HostedConnection;
import com.jme3.network.Network;
import com.jme3.network.Server;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.sandobox.server.Constants;
import net.omega.sandobox.server.CryptoManager;
import net.omega.sandobox.server.config.ConfigurationFactory;
import net.omega.sandobox.server.game.data.ClientInstance;
import net.omega.sandobox.server.game.entity.EntityManager;
import com.omega.sandobox.network.game.message.MessageRegister;
import com.omega.sandobox.network.game.message.ServerMessageListener;

public class ServerFactory {

    private static final Logger logger = Logger.getLogger(ServerFactory.class.getName());
    private Server server;
    private byte[] secureJoinPassword;
    private static ServerFactory instance;

    public ServerFactory() {
        MessageRegister.register();
    }

    public static ServerFactory getInstance() {
        if (instance == null)
            instance = new ServerFactory();

        return instance;
    }

    public void start() {
        int _port = ConfigurationHandler.getInstance().getPort();
        logger.log(Level.INFO, "Starting server on port {0}", _port);
        try {
            server = Network.createServer(Constants.SERVER_NAME, Constants.SERVER_VERSION, _port, _port);
            server.addMessageListener(new ServerMessageListener());
            server.addConnectionListener(new ConnectionListener() {
                @Override
                public void connectionAdded(Server server, HostedConnection connection) {
                    logger.log(Level.INFO, "New connection from {0}", connection.getAddress());
                    ClientInstance _clInstance = new ClientInstance();
                    connection.setAttribute("clientInstance", _clInstance);
                }

                @Override
                public void connectionRemoved(Server server, HostedConnection connection) {
                    logger.log(Level.INFO, "Connection from {0} lost", connection.getAddress());
                    ((ClientInstance) connection.getAttribute("clientInstance")).destroyInstance(connection);
                }
            });
            server.start();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Unable to create server on port {0}", _port);

            return;
        }

        logger.log(Level.INFO, "Secure server join password");
        secureJoinPassword = CryptoManager.getInstance().encrypt(
                ConfigurationFactory.getInstance().getProperty("server.password.join"),
                ConfigurationFactory.getInstance().getProperty("server.name"));
        if (secureJoinPassword == null)
            logger.log(Level.INFO, "No password to secure");

        EntityManager.getInstance().initialize(server);
    }

    public void close() {
        server.close();
    }

    public byte[] getJoinPassword() {
        return secureJoinPassword;
    }
}
