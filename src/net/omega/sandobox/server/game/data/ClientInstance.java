package net.omega.sandobox.server.game.data;

import com.jme3.network.HostedConnection;
import net.omega.sandobox.server.config.ConfigurationHandler;
import net.omega.sandobox.server.game.entity.EntityManager;

/**
 *
 * @author Kyu
 */
public class ClientInstance {

    private ClientAuthorization authorization;
    private ClientEntityHandler entityHandler;

    public ClientInstance() {
        authorization = new ClientAuthorization();
        if(!ConfigurationHandler.getInstance().hasJoinPassword()) {
            authorization.setPasswordVerified(true);
        }
    }

    public ClientAuthorization getAuthorization() {
        return authorization;
    }

    public ClientEntityHandler getEntityHandler() {
        return entityHandler;
    }

    public void initializeEntityHandler(HostedConnection connection) {
        entityHandler = new ClientEntityHandler(EntityManager.getInstance().getLocalEntityData());
        entityHandler.createHostedEntity(connection);
    }

    public void destroyInstance(HostedConnection connection) {
        if (entityHandler != null)
            entityHandler.removeHostedEntity(connection);
    }
}
