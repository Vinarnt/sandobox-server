package net.omega.sandobox.server.game.data;

/**
 *
 * @author Kyu
 */
public enum GameMode {

    TeamDeathMatch(1, "Team Death Match"),
    FreeForAll(2, "Free For All"),
    CaptureTheFlag(3, "Capture The Flag");
    
    private int id;
    private String name;

    GameMode(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static GameMode getGameModeById(int id) {
        for (GameMode _mode : values()) {
            if (_mode.getId() == id)
                return _mode;
        }

        return null;
    }
}
