package net.omega.sandobox.server.game.data;

import static net.omega.sandobox.server.game.data.GameMode.values;

/**
 *
 * @author Kyu
 */
public enum GameMap {

    TestMap(1, "TestMap");
    private int id;
    private String name;

    GameMap(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static GameMap getGameMapById(int id) {
        for (GameMap _map : values()) {
            if (_map.getId() == id)
                return _map;
        }

        return null;
    }
}
