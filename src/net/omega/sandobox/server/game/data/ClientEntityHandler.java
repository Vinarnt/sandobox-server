package net.omega.sandobox.server.game.data;

import com.jme3.network.HostedConnection;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntityId;
import net.omega.sandobox.server.config.ConfigurationHandler;
import net.omega.sandobox.server.game.entity.EntityManager;
import net.omega.sandobox.server.game.entity.component.NetworkComponent;
import net.omega.sandobox.server.master.MasterClientFactory;
import net.omega.sandobox.server.master.packet.ServerUpdatePlayerPacket;

/**
 *
 * @author Kyu
 */
public class ClientEntityHandler {

    private EntityData entityData;
    private EntityId clientEntity;

    public ClientEntityHandler(EntityData ed) {
        entityData = ed;
    }

    public void createHostedEntity(HostedConnection connection) {
        clientEntity = entityData.createEntity();
        entityData.setComponent(clientEntity, new NetworkComponent(connection));

        MasterClientFactory.getInstance().send(
                new ServerUpdatePlayerPacket(
                EntityManager.getInstance().getPlayerCount(),
                ConfigurationHandler.getInstance().getMaxPlayers()));
    }
    
    public void removeHostedEntity(HostedConnection connection) {
        entityData.removeEntity(clientEntity);
        MasterClientFactory.getInstance().send(
                new ServerUpdatePlayerPacket(
                EntityManager.getInstance().getPlayerCount(),
                ConfigurationHandler.getInstance().getMaxPlayers()));
    }

    public EntityId getId() {
        return clientEntity;
    }
}
