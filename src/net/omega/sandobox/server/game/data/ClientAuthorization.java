package net.omega.sandobox.server.game.data;

/**
 *
 * @author Kyu
 */
public class ClientAuthorization {

    private boolean passwordVerified;

    public ClientAuthorization() {
    }

    public boolean isPasswordVerified() {
        return passwordVerified;
    }

    public void setPasswordVerified(boolean passwordVerified) {
        this.passwordVerified = passwordVerified;
    }
}
