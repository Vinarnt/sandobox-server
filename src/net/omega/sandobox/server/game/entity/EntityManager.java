package net.omega.sandobox.server.game.entity;

import com.jme3.network.Server;
import com.simsilica.es.EntityData;
import com.simsilica.es.EntitySet;
import com.simsilica.es.base.DefaultEntityData;
import com.simsilica.es.net.EntitySerializers;
import com.simsilica.es.server.EntityDataHostService;
import java.util.logging.Logger;
import net.omega.sandobox.server.game.entity.component.NetworkComponent;

/**
 *
 * @author Kyu
 */
public class EntityManager {

    private static final Logger logger = Logger.getLogger(EntityManager.class.getName());
    private EntityDataHostService remoteEntityData;
    private EntityData localEntityData;
    private static EntityManager instance;

    public EntityManager() {
    }

    public static EntityManager getInstance() {
        if (instance == null)
            instance = new EntityManager();

        return instance;
    }

    public void initialize(Server server) {
        logger.info("Intialize entity system");
        //EntitySerializers.initialize();
        localEntityData = new DefaultEntityData();
        remoteEntityData = new EntityDataHostService(server, 0, localEntityData);
    }

    public EntityData getLocalEntityData() {
        return localEntityData;
    }

    public int getPlayerCount() {
        EntitySet _set = EntityManager.getInstance().getLocalEntityData().getEntities(NetworkComponent.class);
        int _result = _set.size();
        _set.release();

        return _result;
    }
}
