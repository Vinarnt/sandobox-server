package net.omega.sandobox.server.game.entity.component;

import com.jme3.network.HostedConnection;
import com.simsilica.es.EntityComponent;

/**
 *
 * @author Kyu
 */
public class NetworkComponent implements EntityComponent {
    
    private HostedConnection connection;

    public NetworkComponent(HostedConnection connection) {
        this.connection = connection;
    }

    public HostedConnection getConnection() {
        return connection;
    }
    
}
