package net.omega.sandobox.server.game;

import net.omega.sandobox.server.config.ConfigurationHandler;
import com.jme3.app.SimpleApplication;
import net.omega.sandobox.server.master.MasterClientFactory;

/**
 *
 * @author Kyu
 */
public class ServerCore extends SimpleApplication {

    private boolean isDedicated;

    public ServerCore(boolean isDedicated) {
        this.isDedicated = isDedicated;
    }

    @Override
    public void simpleInitApp() {
        ConfigurationHandler.getInstance().initialize();
        ConfigurationHandler.getInstance().setIsDedicated(isDedicated);

        ServerFactory.getInstance().start();
        MasterClientFactory.getInstance().connect();
    }

    @Override
    public void destroy() {
        ServerFactory.getInstance().close();

        super.destroy();
    }
}
