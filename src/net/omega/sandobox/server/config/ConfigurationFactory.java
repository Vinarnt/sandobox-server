/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.omega.sandobox.server.config;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.sandobox.server.Constants;

/**
 *
 * @author Kyu
 */
public class ConfigurationFactory {

    private static final Logger logger = Logger.getLogger(ConfigurationFactory.class.getName());
    private Properties properties;
    
    private static ConfigurationFactory instance;
    private String[] allowedNulls = new String[]{
        "server.password.join",
        "server.password.admin"
    };

    public ConfigurationFactory() {
    }

    public static ConfigurationFactory getInstance() {
        if (instance == null)
            instance = new ConfigurationFactory();

        return instance;
    }

    public void initialize() {
        logger.info("Initialize server configuration");
        properties = new Properties();
        try {
            FileInputStream _stream = new FileInputStream(Constants.PATH_SERVER_CONFIG);
            properties.load(_stream);
            _stream.close();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "File {0} not found", Constants.PATH_SERVER_CONFIG);
            System.exit(-1);
        }
        for (Object _key : properties.keySet()) {
            String _strKey = (String) _key;
            String _value = properties.getProperty(_strKey);
            if (_value.isEmpty()) {
                boolean _canBeNull = false;
                for (String _allowedNull : allowedNulls) {
                    if (_allowedNull.equals(_key)) {
                        _canBeNull = true;
                        break;
                    }
                }
                if (!_canBeNull) {
                    logger.log(Level.SEVERE, "The property {0} should not be empty", _strKey);
                    System.exit(-1);
                }
            }
        }
    }

    public String getProperty(String key) {
        String _value = properties.getProperty(key);
        if (_value == null) {
            logger.log(Level.SEVERE, "The key {0} is missing", key);
            System.exit(-1);
        }

        return _value;
    }

    public void setProperty(String key, String value) {
        properties.setProperty(key, value);

        try {
            OutputStream _outStream = new FileOutputStream(Constants.PATH_SERVER_CONFIG);
            properties.store(_outStream, "Server configuration file");
            _outStream.close();
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Unable to save server configuration");
        }
    }
}
