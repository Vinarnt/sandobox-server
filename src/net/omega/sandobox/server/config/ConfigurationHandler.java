package net.omega.sandobox.server.config;

/**
 *
 * @author Kyu
 */
public class ConfigurationHandler {

    private String serverName;
    private int port;
    private boolean isDedicated;
    private boolean hasJoinPassword;
    private String joinPassword;
    private String adminPassword;
    private short maxPlayers;
    private short gameMode;
    private short map;
    private static ConfigurationHandler instance;
    private ConfigurationFactory configFactory;

    public ConfigurationHandler() {
    }

    public static ConfigurationHandler getInstance() {
        if (instance == null)
            instance = new ConfigurationHandler();

        return instance;
    }

    public void initialize() {
        configFactory = ConfigurationFactory.getInstance();
        configFactory.initialize();

        serverName = configFactory.getProperty("server.name");
        port = Integer.valueOf(configFactory.getProperty("server.port"));
        joinPassword = configFactory.getProperty("server.password.join");
        hasJoinPassword = !joinPassword.isEmpty();
        adminPassword = configFactory.getProperty("server.password.admin");
        maxPlayers = Short.valueOf(configFactory.getProperty("game.players.max"));
        gameMode = Short.valueOf(configFactory.getProperty("game.mode"));
        map = Short.valueOf(configFactory.getProperty("game.map"));
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
        configFactory.setProperty("server.name", serverName);
    }

    public int getPort() {
        return port;
    }

    public boolean isDedicated() {
        return isDedicated;
    }

    public void setIsDedicated(boolean isDedicated) {
        this.isDedicated = isDedicated;
    }

    public boolean hasJoinPassword() {
        return hasJoinPassword;
    }

    public String getJoinPassword() {
        return joinPassword;
    }

    public void setJoinPassword(String joinPassword) {
        this.joinPassword = joinPassword;
        this.hasJoinPassword = !joinPassword.isEmpty();
        configFactory.setProperty("server.password.join", joinPassword);
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
        configFactory.setProperty("server.password.admin", adminPassword);
    }

    public short getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(short maxPlayers) {
        this.maxPlayers = maxPlayers;
        configFactory.setProperty("game.players.max", String.valueOf(maxPlayers));
    }

    public short getGameMode() {
        return gameMode;
    }

    public void setGameMode(short gameMode) {
        this.gameMode = gameMode;
        configFactory.setProperty("game.mode", String.valueOf(gameMode));
    }

    public short getMap() {
        return map;
    }

    public void setMap(short map) {
        this.map = map;
        configFactory.setProperty("game.map", String.valueOf(map));
    }
}
