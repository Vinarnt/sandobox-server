package net.omega.sandobox.server.master.codec;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class PacketDecoder extends MessageToMessageDecoder<ByteBuf> {

    private static final Logger logger = Logger.getLogger(PacketDecoder.class.getName());

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        try {
            ByteBufInputStream _buffer = new ByteBufInputStream(in);
            byte _packetID = _buffer.readByte();
            switch (_packetID) {



            }
            _buffer.close();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Error on ByteBufInputStream", ex);

        }
    }
}
