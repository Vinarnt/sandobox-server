/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.omega.sandobox.server.master.codec;

import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.sandobox.server.master.packet.AbstractPacket;

/**
 *
 * @author Kyu
 */
public class PacketEncoder extends MessageToMessageEncoder<AbstractPacket> {

    private static final Logger logger = Logger.getLogger(PacketEncoder.class.getName());
    
    @Override
    protected void encode(ChannelHandlerContext ctx, AbstractPacket packet, List<Object> out) throws Exception {
        logger.log(Level.INFO, "To encode {0}", packet.getClass().getSimpleName());
        Channel _channel = ctx.channel();
        packet.encode(_channel);
        ByteBufOutputStream _out = packet.getOut();
        if(_out != null)
        {
            out.add(_out.buffer());
            _out.close();
        }
    }
    
}
