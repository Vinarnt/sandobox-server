package net.omega.sandobox.server.master;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.util.logging.Logger;
import net.omega.sandobox.server.master.packet.AbstractPacket;

/**
 *
 * @author Kyu
 */
public class MasterClientHandler extends SimpleChannelInboundHandler<AbstractPacket> {

    private static final Logger logger = Logger.getLogger(MasterClientHandler.class.getName());

    public MasterClientHandler() {
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, AbstractPacket packet) throws Exception {
        Channel _channel = ctx.channel();
        packet.execute(_channel);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        
        MasterClientFactory.getInstance().connect();
    }
    
}
