package net.omega.sandobox.server.master;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import net.omega.sandobox.server.master.codec.PacketDecoder;
import net.omega.sandobox.server.master.codec.PacketEncoder;

/**
 *
 * @author Kyu
 */
public class MasterClientInitializer extends ChannelInitializer<SocketChannel> {

    public MasterClientInitializer() {
    }

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        ChannelPipeline _pipeline = channel.pipeline();
        
        _pipeline.addLast("logHandler", new LoggingHandler(LogLevel.INFO));
        _pipeline.addLast("decoder", new PacketDecoder());
        _pipeline.addLast("encoder", new PacketEncoder());
        _pipeline.addLast("handler", new MasterClientHandler());
    }
    
}
