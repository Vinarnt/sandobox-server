package net.omega.sandobox.server.master;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.sandobox.server.Constants;
import net.omega.sandobox.server.master.packet.AbstractPacket;
import net.omega.sandobox.server.master.packet.RegisterPacket;
import org.apache.log4j.BasicConfigurator;

/**
 *
 * @author Kyu
 */
public class MasterClientFactory {

    private static final Logger logger = Logger.getLogger(MasterClientFactory.class.getName());
    private EventLoopGroup workerGroup;
    private Bootstrap bootstrap;
    private ChannelFuture channelFuture;
    private ChannelFutureListener futureListener;
    private static MasterClientFactory instance;

    public MasterClientFactory() {
        BasicConfigurator.configure();

        workerGroup = new NioEventLoopGroup();
        bootstrap = new Bootstrap();
        bootstrap.group(workerGroup);
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        bootstrap.handler(new MasterClientInitializer());
        futureListener = new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) {
                Channel _channel = future.channel();
                if (future.isSuccess()) {
                    logger.info("Connected to master server");
                    send(new RegisterPacket());
                } else {
                    _channel.close();
                    int _delay = Constants.MASTER_SERVER_RECONNECT_DELAY;
                    logger.log(Level.INFO, "Attempt to connect within {0} seconds", _delay);
                    _channel.eventLoop().schedule(new Runnable() {
                        @Override
                        public void run() {
                            connect();
                        }
                    }, _delay, TimeUnit.SECONDS);

                }
            }
        };
    }

    public static MasterClientFactory getInstance() {
        if (instance == null)
            instance = new MasterClientFactory();

        return instance;
    }

    public void connect() {
        if (channelFuture != null && channelFuture.channel().isOpen())
            return;

        channelFuture = bootstrap.connect(Constants.MASTER_SERVER_HOST, Constants.MASTER_SERVER_PORT).addListener(futureListener);
    }

    public void close() {
        if (channelFuture != null && channelFuture.channel().isOpen()) {
            try {
                channelFuture.channel().disconnect().sync();
            } catch (InterruptedException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        logger.info("Shutdown master server thread");
        workerGroup.shutdownGracefully();
    }

    public void send(AbstractPacket packet) {
        logger.log(Level.INFO, "Send >> {0}", packet.toString());
        channelFuture.channel().writeAndFlush(packet);
    }
}
