package net.omega.sandobox.server.master.packet;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class ServerUpdatePlayerPacket extends AbstractPacket {

    private static final Logger logger = Logger.getLogger(ServerUpdatePlayerPacket.class.getName());
    public static final byte PACKET_ID = 0x4;
    private int currentPlayers;
    private int maxPlayers;

    public ServerUpdatePlayerPacket(int currentPlayers, int maxPlayers) {
        this.currentPlayers = currentPlayers;
        this.maxPlayers = maxPlayers;
    }

    @Override
    public void decode(ByteBufInputStream buffer, Channel channel) {
    }

    @Override
    public void execute(Channel channel) {
    }

    @Override
    public void encode(Channel channel) {
        out = new ByteBufOutputStream(Unpooled.buffer(64));
        try {
            out.writeByte(PACKET_ID);
            out.writeInt(currentPlayers);
            out.writeInt(maxPlayers);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Unable to build packet", ex);
        }
    }
}
