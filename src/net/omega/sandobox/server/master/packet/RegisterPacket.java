package net.omega.sandobox.server.master.packet;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.sandobox.server.config.ConfigurationHandler;
import net.omega.sandobox.server.game.data.GameMap;
import net.omega.sandobox.server.game.data.GameMode;

/**
 *
 * @author Kyu
 */
public class RegisterPacket extends AbstractPacket {

    private static final Logger logger = Logger.getLogger(RegisterPacket.class.getName());
    public static final byte PACKET_ID = 0x3;
    
    @Override
    public void decode(ByteBufInputStream buffer, Channel channel) {
    }

    @Override
    public void execute(Channel channel) {
    }

    @Override
    public void encode(Channel channel) {
        try {
            ConfigurationHandler _config = ConfigurationHandler.getInstance();
            out = new ByteBufOutputStream(Unpooled.buffer(1024));
            out.writeByte(PACKET_ID);
            out.writeUTF(_config.getServerName());
            out.writeInt(_config.getPort());
            out.writeBoolean(_config.isDedicated());
            out.writeBoolean(!_config.getJoinPassword().isEmpty());
            out.writeInt(_config.getMaxPlayers());
            out.writeUTF(GameMode.getGameModeById(_config.getGameMode()).getName());
            out.writeUTF(GameMap.getGameMapById(_config.getMap()).getName());
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Unable to encode packet", ex);
        }
    }
    
}
